/*
  MonkeyBeans2 Copyright (C) 2000 �RDI Gerg� <cactus@cactus.rulez.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 2
  as published by the Free Software Foundation.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

package org.GNOME.Bonobo;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import java.util.*;

public class UnknownImpl implements UnknownOperations
{
    Set interfaces = new HashSet();
    UnknownImpl outer = null;
    int refcount = 0;
    public org.omg.PortableServer.Servant tie;
    public org.omg.PortableServer.POA poa = null;
    org.omg.CORBA.Object objRef = null;

    /*
     * ref/unref: We'll just NOP it for now
     */
    public synchronized void ref ()
	{
	    refcount++;
	}

    public synchronized void unref ()
	{
	    refcount--;

	    if (refcount < 0)
	    {
		error ("Bonobo::UnknownImpl: Negative refcount reached!");
		refcount = 0;
	    }
	    
	    if (refcount == 0)
	    {
		// Die
		destroy ();
	    }
	}

    /*
     * queryInterface:
     */
    public Unknown queryInterface (String repo_id)
	{
//	    System.out.println ("calling queryInterface for " + repo_id + " outer " + outer);
	    if (outer != null)
		return outer.queryInterface (repo_id);

//	    System.out.println ("match " + tie()._is_a(repo_id));
	    if (tie._is_a (repo_id)) {
            Unknown tmp_obj = tie();
            tmp_obj.ref();
            return tmp_obj;
        }

	    Iterator i = interfaces.iterator();
	    while(i.hasNext())
	    {
            Unknown curr_obj = ((UnknownImpl) i.next ()).tie();
            if (curr_obj._is_a (repo_id))
            {
                curr_obj.ref ();
                return curr_obj;
            }
	    }
	    
	    return null;
	}

    void error (String message)
	{
	    System.err.println ("*** MonkeyBeans  ERROR  ***: " +
				this + ": " + message);
	}

    void warning (String message)
	{
	    System.err.println ("*** MonkeyBeans WARNING ***: " +
				this + ": " + message);
	}
    
    /*
     * add_interface: Merge new_iface's aggregates into the object
     */
    public void add_interface (UnknownImpl new_iface)
	{
	    if (refcount != 1)
	    {
		error ("Post-activation aggregation requested!");
	    }
	    
	    // Merge the two aggregates
	    interfaces.add (new_iface);
	    new_iface.outer = this;
	}

    public Unknown tie ()
	{
	    if (objRef == null) {
            // System.out.println("tie( " + this.toString() + " ) : objRef == null");
	        try {
                objRef = UnknownHelper.narrow (poa.servant_to_reference(tie));
	        } catch (Exception e) {
		        System.out.println (e);
	        }
	    } 
        // System.out.println("tie( " + this.toString() + " ) : returning : " + ((Unknown)objRef).toString() );
        // System.out.println("tie( " + this.toString() + " ) : returning : " + Integer.toHexString(((Unknown)objRef).hashCode() ) );
 
	    return (Unknown) objRef;
	}
    
    public UnknownImpl (ORB orb, POA poa)
	{
	    this ();
	    tie = new UnknownPOATie (this);	
	    try {
		    poa.activate_object(tie);
	    } catch (Exception e) {
		    System.out.println (e);
	    }
	    this.poa = poa;
	}
    
    public UnknownImpl ()
	{
	    refcount = 1;
	}

    void destroy ()
	{
	}
}
