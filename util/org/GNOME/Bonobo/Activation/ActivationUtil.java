/*
 * MonkeyBeans2 for GNOME
 * Copyright 2002 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
package org.GNOME.Bonobo.Activation;
import org.GNOME.Bonobo.*;
import org.omg.PortableServer.*;
import org.omg.CORBA.*;
import java.io.*;

public class ActivationUtil {

	static ORB orb = null;
	static POA rootpoa = null;

	public static boolean requiredJREVersionFound () {
		String vm_rev = System.getProperty("java.version");
		if (vm_rev.compareTo("1.4.0") < 0) {
			System.out.println("WARNING: TestAT requires " +
					   "JVM version 1.4.0 or greater.");
			return false;
		} else {
			return true;
		}
	}

	public static String activationIORString (String activationQuery) {
		String iorString = null;
		try {
			Process p = Runtime.getRuntime().exec("activation-client -s repo_ids.has('"
							      + activationQuery + "')");
			BufferedReader b = new BufferedReader (
				new InputStreamReader (p.getInputStream ()));
			b.readLine(); // throw away first line...
 			String result = b.readLine();
  			if (result.indexOf("RESULT_OBJECT") == -1) {
 				throw new AssertionError (
	 				activationQuery + " could not be activated.");
		 	}
			String output = b.readLine();
			int index = output.indexOf("IOR:");
			iorString = output.substring(index);
			p.waitFor();
		} catch (Exception e) {
 			e.printStackTrace();
		}
		return iorString;
	}

	public static org.omg.CORBA.Object activationObjectReference (String activationQuery) { 
		org.omg.CORBA.Object objref = null;
		try {
			String iorString = activationIORString (activationQuery);
			// System.err.println("Proxy IOR: " + iorString );
			objref = getORB().string_to_object (iorString);
		} catch (Exception e) {
			System.err.println ("Could not obtain object reference for " +
					    activationQuery);
		}
		return objref;
	}

	public static ORB getORB () {
		if (orb == null) {
			try {
				String[] args = {""};
				orb = ORB.init (args, null);
			} catch (Exception ex) {
				ex.printStackTrace ();
			}
		}
		return orb;
	}

	public static POA getRootPOA () {
		if (rootpoa == null) {
			try {
			    rootpoa = (POA) 
				getORB().resolve_initial_references ("RootPOA");
			    rootpoa.the_POAManager().activate();
			} catch (Exception ex) {
			    ex.printStackTrace ();
			}
		}
		return rootpoa;
	}	
}
